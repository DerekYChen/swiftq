package com.example.adi.swiftq;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class UserProfileActivity extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);

        Toolbar toolBar = (Toolbar) findViewById(R.id.toolBar);
        toolBar.setTitle("User Profile");
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onResume() {
        //get current xp
        SharedPreferences pref = getSharedPreferences(PREFS_NAME,0);
        xp = pref.getInt("xp",0);
        TextView xpText = (TextView) findViewById(R.id.xp);
        xpText.setText("EXP: " + String.valueOf(xp));

        ProgressBar XPBar = (ProgressBar) findViewById(R.id.XPBar);
        TextView levelText = (TextView) findViewById(R.id.level);
        int level = (int) (Math.sqrt(40*xp+100)+10)/20;
        levelText.setText("Level: "+String.valueOf(level));
        XPBar.setMax(10*level*(level+1));
        XPBar.setProgress(xp);

        super.onResume();
    }

}
