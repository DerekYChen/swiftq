package com.example.adi.swiftq;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ImageButton;

import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity {

    GoogleMap googleMap;

    //Google place picker variables
    private static final int PLACE_PICKER_REQUEST = 1;
    private CharSequence mPlaceID = "null";
    private CharSequence mNameText = "null";

    private static final int INPUT_ACTIVITY_REQUEST = 2;

    //TODO: Alan - this is only for testing, should retrieve coords later on
    private static final LatLngBounds UBC_COORD = new LatLngBounds(
            new LatLng(49.2665635, -123.2427695), new LatLng(49.2665635, -123.2427695));

    //Shared pref + gamification
    public static final String PREFS_NAME = "Gamification";
    public int xp = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //set content view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_picker);

        //Setting up toolbar
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolBar);
        toolBar.setElevation(25);
        setSupportActionBar(toolBar);

        // picker button on click goes to place picker
        Button pickerButton = (Button) findViewById(R.id.pickerButton);
        pickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                    intentBuilder.setLatLngBounds(UBC_COORD);
                    Intent intent = intentBuilder.build(MainActivity.this);
                    //Intent intent = intentBuilder.build(getApplicationContext());
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        ImageButton profileButton = (ImageButton) findViewById(R.id.userProfile);
        profileButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, UserProfileActivity.class);
                startActivity(intent);
            }
        });



//
//        Button profileButton = (Button) findViewById(R.id.profileButton);
//        outputButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                //setContentView(R.layout.input);
//                Intent intent = new Intent(MainActivity.this, UserProfileActivity.class);
//                startActivity(intent);
//            }
//        });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PLACE_PICKER_REQUEST:
                    // Get place information
                    final Place place = PlacePicker.getPlace(data, this);
                    final CharSequence name = place.getName();
                    final CharSequence placeid = place.getId();

                    mPlaceID = placeid;
                    mNameText = name;

                    String attributions = PlacePicker.getAttributions(data);
                    if (attributions == null) {
                        attributions = "";
                    }

                    Intent intent = new Intent(MainActivity.this, SelectActivity.class);
                    intent.putExtra("nameText", mNameText);
                    intent.putExtra("placeID", mPlaceID);
                    startActivityForResult(intent, INPUT_ACTIVITY_REQUEST);

                    break;
                case INPUT_ACTIVITY_REQUEST:

                    break;
                default:
                    super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setXP() {
        Log.i("xp",String.valueOf(xp));
        SharedPreferences pref = getSharedPreferences(PREFS_NAME,0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("xp",xp);
        editor.commit();
    }

}
