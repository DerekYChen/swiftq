package com.example.adi.swiftq;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class SelectActivity extends MainActivity {

    String placeName;
    String placeID;
    Integer time;
    String baseURL = "http://hub.urbanopus.net/wotkit/api";

    private TextView mPlaceName;
    private TextView mPlaceIDView;

    private static final int INPUT_ACTIVITY_REQUEST = 2;

    static final long MILLIS_IN_DAY = 86400000;
    static final long MILLIS_IN_HOUR = 3600000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();

        // Passed in values
        Bundle bundle = getIntent().getExtras();
        placeName = bundle.getString("nameText");
        placeID = bundle.getString("placeID");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.select);

        // Setting up toolbar
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolBar);
        toolBar.setTitle("Choose Action");
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Setting views
        mPlaceName = (TextView) findViewById(R.id.restName);
        mPlaceName.setText(placeName);
        mPlaceIDView = (TextView) findViewById(R.id.placeIDView);
        mPlaceIDView.setText(placeID);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // input button on opens input dialog
        Button inputButton = (Button) findViewById(R.id.inputButton);
        inputButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                // Show alertDialog when pressed
                showInputAlert();

            }
        });

        Button outputButton = (Button) findViewById(R.id.outputButton);
        outputButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                // Show alertDialog when pressed
                showOutputAlert();
            }
        });

    }

    // Function to show alertDialog for input selection
    private void showInputAlert() {

        //custom dialog
        final Dialog dialog = new Dialog(this, R.style.AlertDialogCustom);
        dialog.setContentView(R.layout.input_dialog);

        // set title and dialog shape
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle(R.string.inputDialogTitle);

        // submit and cancel button
        Button dialogButtonCancel = (Button) dialog.findViewById(R.id.customDialogCancel);
        Button dialogButtonOk = (Button) dialog.findViewById(R.id.customDialogOk);

        // Click cancel to dismiss android custom dialog box
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // get time
        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText timeInput = (EditText) dialog.findViewById(R.id.timeInput);
                time = Integer.parseInt(timeInput.getText().toString());

                //comment to test
                if (placeID.length() == 0) placeID = "testid1";

                Integer id = GetSensorID(placeID);
                Log.i("placeID", placeID);
                Log.i("sensor id", id.toString());
                if (id == -1) {
                    CreateNewSensor(placeID);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Integer id = GetSensorID(placeID);
                            InsertData(id, time);
                        }
                    }, 500);

                } else {
                    InsertData(id, time);
                }

                SharedPreferences pref = getSharedPreferences(PREFS_NAME, 0);
                xp = pref.getInt("xp",0);
                xp += 5;
                setXP();

                dialog.dismiss();
            }
        });

        // show dialog
        dialog.show();

    }

    // Function to show alertDialog for Output
    private void showOutputAlert() {

        //custom dialog
        final Dialog dialog = new Dialog(this, R.style.AlertDialogCustom);
        dialog.setContentView(R.layout.output_dialog);

        // set title and dialog shape
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle(R.string.outputDialogTitle);

        // get and cancel button
        Button dialogButtonCancel = (Button) dialog.findViewById(R.id.customDialogCancel);
        Button dialogButtonOk = (Button) dialog.findViewById(R.id.customDialogOk);

        // Click cancel to dismiss android custom dialog box
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // Submit button to input time
        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView output = (TextView) dialog.findViewById(R.id.timeEstimate);

                //http://hub.urbanopus.net/wotkit/groups/91700
                JSONObject jsonParams = new JSONObject();

                Integer id = GetSensorID(placeID);

                if (id != -1) {
                    Long time= System.currentTimeMillis();
                    Long duration = 2*MILLIS_IN_HOUR;

                    ArrayList<Integer> waitTimeData = new ArrayList<Integer>();

                    for (int i=0; i<4; i++) {
                        Long startTime = time - MILLIS_IN_HOUR - i*MILLIS_IN_DAY*7;
                        String query = "/sensors/"+Integer.toString(id)+"/data?start="+Long.toString(startTime)+"&after="+Long.toString(duration);
                        JSONArray resp = sendHTTPRequest(baseURL +query,"GET",jsonParams);
                        Log.i("response",resp.toString());
                        if (resp.length()>0) {
                            try {
                                for (int j=0; j<resp.length(); j++) {
                                    waitTimeData.add(resp.getJSONObject(j).getInt("value"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    if (waitTimeData.size() > 0) {
                        int waitTimeSum = 0;
                        for (int waitTime : waitTimeData) {
                            waitTimeSum += waitTime;
                        }
                        Integer waitTimeEstimate = (waitTimeSum+waitTimeData.size()/2)/waitTimeData.size();
                        output.setText(Integer.toString(waitTimeEstimate) + " Minutes");
                    } else {
                        output.setText("Sorry, not enough data!");
                    }
                } else {
                    CreateNewSensor(placeID);
                }

            }
        });

        // show dialog
        dialog.show();

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        //intent.putExtra("input", inp);
        setResult(RESULT_OK, intent);

        super.onBackPressed();

    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case INPUT_ACTIVITY_REQUEST:
                    break;
                default:
                    super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    /*
    URBAN OPUS DATAHUB API FUNCTIONS
     */

    //Send HTTP GET/POST requests and parse output properly
    public JSONArray sendHTTPRequest(String urlString, String type, JSONObject JSONData) {
        URL url;
        HttpURLConnection connection = null;
        String basicAuth = "Basic " + Base64.encodeToString("enph479:engphysadi".getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
        JSONArray response = new JSONArray();
        String responseString = "nada";
        try {
            //Create connection
            url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod(type);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", basicAuth);
            connection.setRequestProperty("Host", "hub.urbanopus.net");
            connection.connect();

            //Send request
            Log.i("json request", JSONData.toString());

            if (type == "POST") {
                BufferedWriter os = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
                os.write(JSONData.toString());
                os.close();
            }

            Integer responseCode = connection.getResponseCode();
            Log.i("HTTP RESPONSE",Integer.toString(responseCode));

            //Get response
            InputStream is = null;
            try{
                is = connection.getInputStream();
            } catch (IOException e) {
                is = connection.getErrorStream();
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = br.readLine()) != null) {
                sb.append(line +'\n');
            }
            responseString = sb.toString();
            Log.i("Response String",responseString);
            Boolean valid = true;

            try {
                new JSONArray(responseString);
            } catch (JSONException ex1) {
                valid =  false;
            }

            if (valid) response = new JSONArray(responseString);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(connection != null) {
                connection.disconnect();
            }
        }
        return response;
    }


    //Using placeID (a sensor field) find the sensor ID
    public Integer GetSensorID(String placeID) {
        Integer sensorID;
        String query = "/sensors?metadata=placeID:" + placeID;
        JSONObject jsonParams = new JSONObject();
        JSONArray resp = sendHTTPRequest(baseURL+query,"GET",jsonParams);
        try {
            sensorID = resp.getJSONObject(0).getInt("id");
        } catch (JSONException e) {
            sensorID = -1;
        }
        Log.i("GetSensorID:","pid: "+placeID+" sid: "+sensorID);
        return sensorID;
    }

    //Create a new sensor with field placeID=x
    public void CreateNewSensor(String placeID) {
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("name", "restaurant-" + placeID.toLowerCase());
            jsonParams.put("longName", "restaurant-" + placeID.toLowerCase());
            jsonParams.put("visibility", "PUBLIC");
            jsonParams.put("description","test restaurant");
            JSONArray tags = new JSONArray();
            tags.put("restaurant");
            jsonParams.put("tags",tags);
            JSONObject metadata = new JSONObject();
            metadata.put("placeID",placeID);
            jsonParams.put("metadata",metadata);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("CreateNewSensor", placeID);
        sendHTTPRequest(baseURL + "/sensors/", "POST", jsonParams);

        //Adding sensor to group
        final String pid = placeID;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Integer id = GetSensorID(pid);
                Log.i("AddSensorToGroup", pid);
                JSONObject params = new JSONObject();
                sendHTTPRequest(baseURL + "/groups/91700/sensors/" + id, "POST", params);
            }
        }, 500);

        return;
    }

    //Add wait time data to a sensor
    public void InsertData(Integer id, Integer time) {
        String query = "/sensors/" + Integer.toString(id) + "/data";
        JSONObject data = new JSONObject();
        try {
            data.put("value", time);
        } catch(Exception e) {
            e.printStackTrace();
        }
        Log.i("InsertData",id+" "+time);
        sendHTTPRequest(baseURL + query, "POST", data);
        return;
    }
}
